---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## Instructions

We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Every layer in the organization has an objective that is similar to the key results above it.
So we end up with 6 layers of indentation on this page.
OKRs are prefaced with the owner of the key result.
We use spaces to indent instead of tabs.
The match of one key result with the higher key result doesn't have to be perfect.

## 2017-Q2

Summary: With a great team make a popular next generation product that grows revenue.

* Company: Grow incremental ACV
  * CEO: Ensure the pipeline (SQLs) cover our plan predictably
    * CMO: Identify CE installations within Fortune 1000 or with installations with over 500 users and launch demand generation campaigns
    * CFO: Tight integration between Zuora, SFDC and Zendesk so all contributors have the information they need to perform their job.
    * CRO: Increase New Business Q2 Deal Velocity by 200% YoY to 479
      * Sales Ops: EXAMPLE Measure velocity correctly
    * CMO: 50% of sales quota pipeline generated from inbound marketing activities
    * CMO: Increase EE Trials by 50% and Trial conversion improvement by 50% quarter over quarter
  * CEO: Ensure we understand existing account growth and get it above 200%
    * VPEng: Geo DR succcessful deployments, including GitLab.com.
      * Backend director: Get Geo reliable
         * Platform load: Make a testing framework for Geo.
            * Developer X: Do a better handover to production.
    * Product: CE docs link to EE wherever relevant.
    * VPEng: Fix things that cause churn.
    * CMO: Educational Email Campaign Series - teaching users what is possible within GitLab, how to use it (documentation) and how to get started
* Company: Popular next generation product
  * CEO: GitLab.com trustworthy (people noticing that it is much better)
  * CEO: 100% increased usage of idea to production
